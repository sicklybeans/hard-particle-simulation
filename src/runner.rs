use crate::test_cases::*;

pub fn go() {
  // test_moving_wall_error().run("output/test-moving-wall-error.hdf5");
  // test_moving_wall_single_particles().run("output/test-moving-wall-single-particles.hdf5");
  // test_moving_wall_medium_gas().run("output/test-moving-wall-medium-gas.hdf5");
  test_moving_wall_fast_gas().run("output/test-moving-wall-fast-gas.hdf5");
  // test_2_stationary().run("output/test-2-stationary.hdf5");
  // test_2_stationary_angle().run("output/test-2-stationary-angle.hdf5");
  // test_2_stationary_angle_reverse().run("output/test-2-stationary-angle-reverse.hdf5");
  // test_2_equal_collision_angle().run("output/test-2-equal-collision-angle.hdf5");
  // test_grid_random_velocity().run("output/test-grid-random-velocity.hdf5");
  // test_boundary().run("output/test-boundary.hdf5");
  // test_grid_medium().run("output/test-grid-medium.hdf5");
  // test_gas().run("output/test-gas.hdf5");
  // test_isolated_error().run("output/test-error.hdf5");
}
